#include <stdio.h>

void foo();

int main() {

  printf("This is foo.c\n");

  foo();

  return 0;

}

void foo() {

  printf("Foo Bar Baz\n");

}
