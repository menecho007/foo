#include <stdio.h>
#include <stdlib.h>

#include "feature.h"

// returns rand() func value
int some_random_func();

int main() {

  printf("foo\n");

  // This is a test for git blame
  
#if __linux__
  printf("linux FTW\n");
#else
  printf("Shitty operting system\n");
#endif

  printf("Some random value: %d\n", some_random_func());

  printf("Some of the features: ");

  print_features();

  return 0;

}
// foo bar baz 
// spam comment 2
//

int some_random_func() {
  return rand();
}

