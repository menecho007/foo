build: test

foo:
	$(CC) foo.c

test: feature.o
	$(CC) feature.o test.c -o test.out

kat:
	$(CC) kat.c -o kat

lulcat:
	$(CC) --help | lolcat
